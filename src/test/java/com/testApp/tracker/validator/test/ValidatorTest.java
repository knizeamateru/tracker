package com.testApp.tracker.validator.test;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import com.testApp.tracker.validator.Validator;

public class ValidatorTest {

	@Test
	public void testIsValidSuccess() {
		String input = "CZK 100";
		Validator validator = Validator.getInstance();
		assertTrue(validator.isValid(input));
	}

	@Test
	public void testIsValidFailure() {
		String[] input = { "CZK  100", "CzK 100" };
		Validator validator = Validator.getInstance();
		for (String item : input) {
			assertFalse(validator.isValid(item));
		}
	}

}
