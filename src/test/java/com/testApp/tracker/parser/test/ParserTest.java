package com.testApp.tracker.parser.test;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

import com.testApp.tracker.domain.Payment;
import com.testApp.tracker.reader.Parser;

public class ParserTest {

	@Test
	public void testParseDataSuccess() {
		String temp = "CZK 100";
		Payment payment = new Payment("CZK", new BigDecimal(100));
		Parser parser = new Parser();
		Payment tempPayment = parser.parseData(temp);
		assertEquals(payment, tempPayment);
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void testParseDataFailure(){
		String temp = "CZK100";
		Payment payment = new Payment("CZK", new BigDecimal(100));
		Parser parser = new Parser();
		Payment tempPayment = parser.parseData(temp);
		assertNotEquals(payment, tempPayment);
	}

}
