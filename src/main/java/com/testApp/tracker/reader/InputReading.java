package com.testApp.tracker.reader;

import java.io.BufferedReader;
import java.io.IOException;

import com.testApp.tracker.app.Application;
import com.testApp.tracker.domain.Payment;
import com.testApp.tracker.loader.ConfigKeys;
import com.testApp.tracker.loader.Loader;
import com.testApp.tracker.readerFactory.ReaderFactory;
import com.testApp.tracker.readerFactory.SourceType;
import com.testApp.tracker.storage.Storage;
import com.testApp.tracker.validator.Validator;

/**
 * This implementation is for user inputs (shutting down and payments)
 * 
 * @author
 *
 */
public class InputReading implements Runnable {
	private final static String QUIT =Loader.getInstance().getValue(ConfigKeys.QUIT); 
	private static BufferedReader reader = ReaderFactory.getReaderInstance(null, SourceType.CONSOLE);
	private Storage storage = null;
	private static Validator validator = Validator.getInstance();

	public void setStorage(final Storage storage) {
		this.storage = storage;
	}

	public Storage getStorage() {
		return this.storage;
	}

	@Override
	public void run() {

		while (Boolean.TRUE) {
			String newLine = null;
			try {
				newLine = reader.readLine();
			} catch (IOException e) {
				System.err.println("Something wrong happened during reading user input");
			}
			if (newLine.equalsIgnoreCase(QUIT)) {
			Application.getScheduler().shutdownNow();
				System.exit(0);
			}
			if (validator.isValid(newLine)) {
				System.out.println("Record accepted");
				Parser parser = new Parser();
				Payment tempPayment = parser.parseData(newLine);
				this.storage.store(tempPayment);

			} else {
				System.err.println("Invalid data format/statement.");
			}
		}
	}
}