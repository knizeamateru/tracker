package com.testApp.tracker.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;

import com.testApp.tracker.domain.Payment;
import com.testApp.tracker.readerFactory.ReaderFactory;
import com.testApp.tracker.readerFactory.SourceType;
import com.testApp.tracker.storage.Storage;
import com.testApp.tracker.validator.Validator;

public class Parser {
	private static final String QUIT = "QUIT";
	private Storage storage = null;

	public void setParser(Storage storage) {
		this.storage = storage;
	}

	/**
	 * Loading and creating the payment list
	 * 
	 * @param type
	 *            source type (CONSOLE|TXT_FILE)
	 * @param source
	 *            data source, for TXT type path, for CONSOLE null
	 * @return list of payments
	 */
	public void readData(final SourceType type, final String source) {
		String currentLine = null;
		try (BufferedReader reader = ReaderFactory.getReaderInstance(source, type)) {
			Validator validator = Validator.getInstance();
			while ((currentLine = reader.readLine()) != null) {
				if (validator.isValid(currentLine)) {// only valid data
														// will be parsed
														// and stored
					Payment tempPayment = parseData(currentLine);
					storage.store(tempPayment);
				} else if (currentLine.equalsIgnoreCase(QUIT)) {
					System.exit(0);
				}
			}
		} catch (IOException | NullPointerException e) {
			System.err.println("Something wrong happened during reading data");

		}
	}

	/**
	 * Parsing input string
	 * 
	 * @param data
	 *            input string
	 * @return new Payment object
	 */
	public Payment parseData(final String data) {
		final String result[] = data.split("\\s");
		final String currency = result[0];
		return new Payment(currency, new BigDecimal(result[1]));
	}
}
