package com.testApp.tracker.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.testApp.tracker.loader.ConfigKeys;
import com.testApp.tracker.loader.Loader;

/**
 * 
 * @author
 *
 */
public class Validator {
	public static final String PAYMENT_RECORD_PATTERN = Loader.getInstance().getValue(ConfigKeys.RECORD_PATTERN);
	private static Validator instance = null;
	private static Object mutex = new Object();

	private Validator() {
	}

	public static Validator getInstance() {
		if (instance == null) {
			synchronized (mutex) {
				if (instance == null) {
					instance = new Validator();
				}
			}
		}
		return instance;
	}


	/**
	 * String validation according to regex
	 * 
	 * @param data
	 *            input string
	 * @param regex
	 *            regex
	 * @return TRUE if it is valid, FALSE if not
	 */
	public boolean isValid(final String data) {
		Pattern pattern = Pattern.compile(PAYMENT_RECORD_PATTERN);
		Matcher matcher = pattern.matcher(data);
		return matcher.matches();
	}
	
}