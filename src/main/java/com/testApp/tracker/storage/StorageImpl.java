package com.testApp.tracker.storage;

import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.Map;

import com.testApp.tracker.domain.Payment;
/**
 * Payment storage implemented with the hastable collection
 * @author 
 *
 */
public class StorageImpl implements Storage {
	private Map<String,Payment> tableOfPayment = new Hashtable<String, Payment>(); 
	
	@Override
	public void store(final Payment payment) {
		if(this.tableOfPayment.containsKey(payment.getCurrency())){
			BigDecimal newPaymentValue = this.tableOfPayment.get(payment.getCurrency()).getMoney().add(payment.getMoney());
			this.tableOfPayment.put(payment.getCurrency(), new Payment(payment.getCurrency(),newPaymentValue));
		}else{
			this.tableOfPayment.put(payment.getCurrency(), payment);
		}
	}

	@Override
	public Map<String, Payment> getAll() {
		return this.tableOfPayment;
	}
}
