package com.testApp.tracker.storage;

import java.util.Map;

import com.testApp.tracker.domain.Payment;
/**
 * Interface presents sample methods for operation with Storage
 * @author 
 *
 */
public interface Storage {
	public void store(Payment payment);
	public Map<String, Payment> getAll();
}
