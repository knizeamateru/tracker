package com.testApp.tracker.loader;
/**
 * Mapping between java and config.properties file
 * @author 
 *
 */
public enum ConfigKeys {
	RECORD_PATTERN, QUIT, BASE_CURRENCY, DELIMITER, TIME_INTERVAL, EXCHANGE_RATES, CURRENCY_PATTERN
};