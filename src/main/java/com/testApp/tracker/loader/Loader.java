package com.testApp.tracker.loader;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import com.testApp.tracker.exchangeOffice.ExchangeOffice;

/**
 * Loader for all necessary data from config.properties
 * 
 * @author
 *
 */
public class Loader {

	private static Map<String, String> configValues = new Hashtable<String, String>();
	private final static String CONFIG = "config.properties";
	private final static ClassLoader CL = Loader.class.getClassLoader();
	private static Loader instance = null;
	private static Object mutex = new Object();

	private Loader() {
	}

	public static Loader getInstance() {
		if (instance == null) {
			synchronized (mutex) {
				if (instance == null) {
					instance = new Loader();
					instance.loadConfigData();
				}
			}
		}
		return instance;
	}

	/**
	 * Reading configuration data from config.properties and writing them into
	 * configValues hastable
	 */
	private void loadConfigData() {
		try (InputStream is = CL.getResourceAsStream(CONFIG);) {
			Properties properties = new Properties();
			properties.load(is);
			for (Object item : properties.keySet()) {
				configValues.put(item.toString(), properties.getProperty(item.toString()));
			}

		} catch (IOException e) {
			System.err.println("ERROR during reading config.properties file");
		}
	}

	/**
	 * Reading exchange rates from exchangeRates.properties and writing into
	 * ExchangeOffice
	 * 
	 * @param office
	 *            Exchange office for storing exchange rates
	 */
	public void loadExchangeRates(ExchangeOffice office) {
		final String fileName = getValue(ConfigKeys.EXCHANGE_RATES);
		try (InputStream is = CL.getResourceAsStream(fileName);){

			Properties properties = new Properties();
			properties.load(is);
			for (Object item : properties.keySet()) {
				office.setExchangeRate(item.toString(), new BigDecimal(properties.getProperty(item.toString())));
			}
		} catch (IOException e) {
			System.err.println("ERROR during reading config.properties file");
		}
	}

	public String getValue(ConfigKeys key) {
		return configValues.get(key.toString());
	}

	public Map<String, String> getConfigValues() {
		return configValues;
	}
}