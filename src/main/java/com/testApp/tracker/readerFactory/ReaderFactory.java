package com.testApp.tracker.readerFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;

/**
 * Reader factory
 * @author 
 *
 */
public class ReaderFactory {
	// case of other input source
	public static BufferedReader getReaderInstance(final String path, final SourceType console) {
		BufferedReader reader = null;
		switch (console) {
		case FILE:
			try {
				reader = new BufferedReader(new FileReader(path));
			} catch (FileNotFoundException e) {
				System.err.println(path + "File not found.");
			}
			break;
			
		case CONSOLE:
			reader = new BufferedReader(new InputStreamReader(System.in));
			break;
		default:
			assert false : "Unsupported output type";
			
		}
		return reader;

	}
}
