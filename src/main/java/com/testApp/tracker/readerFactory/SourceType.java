package com.testApp.tracker.readerFactory;
/**
 * Type of input source
 * @author 
 *
 */
public enum SourceType {
	CONSOLE, FILE
};
