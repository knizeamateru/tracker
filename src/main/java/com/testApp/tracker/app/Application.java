package com.testApp.tracker.app;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.testApp.tracker.exchangeOffice.ExchangeOffice;
import com.testApp.tracker.exchangeOffice.ExchangeOfficeImpl;
import com.testApp.tracker.loader.ConfigKeys;
import com.testApp.tracker.loader.Loader;
import com.testApp.tracker.reader.InputReading;
import com.testApp.tracker.reader.Parser;
import com.testApp.tracker.readerFactory.SourceType;
import com.testApp.tracker.storage.Storage;
import com.testApp.tracker.storage.StorageImpl;
import com.testApp.tracker.writer.Listing;

public class Application {

	private static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(2);
	private final static int TIME_INTERVAL = Integer.parseInt(Loader.getInstance().getValue(ConfigKeys.TIME_INTERVAL));

	public static void main(String... args) {
		ExchangeOffice exOffice = new ExchangeOfficeImpl();
		Loader loader = Loader.getInstance();
		loader.loadExchangeRates(exOffice);
		Storage storage = new StorageImpl();
		InputReading inputReadingThread = new InputReading();
		inputReadingThread.setStorage(storage);
		Listing listingThread = new Listing();
		listingThread.setStorage(storage);
		listingThread.setExchangeOffice(exOffice);

	
		if (args.length == 1) {
			Parser parser = new Parser();
			parser.setParser(storage);
			parser.readData(SourceType.FILE, args[0]);
		} else if (args.length > 1) {
			System.err.println("Invalid input");
		}
		System.out.println("Enter new payment record or type 'QUIT' for exit.");

		scheduler.execute(inputReadingThread);
		// Once per minute one item will be displayed
		scheduler.scheduleWithFixedDelay(listingThread, 0, Application.TIME_INTERVAL, TimeUnit.SECONDS);
	}


	public static ScheduledExecutorService getScheduler() {
		return scheduler;
	}
}
