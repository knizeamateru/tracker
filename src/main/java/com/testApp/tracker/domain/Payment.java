package com.testApp.tracker.domain;

import java.math.BigDecimal;

/**
 * Representation of payment (currency and money)
 * 
 * @author
 *
 */
public class Payment {

	private final String currency;
	private BigDecimal money;

	public String getCurrency(){
		return this.currency;
	}
	/**
	 * Constructor
	 * 
	 * @param currency
	 *            currency name
	 * @param money
	 *            amount
	 */
	public Payment(final String currency, final BigDecimal money) {
		this.currency = currency;
		this.money = money;
	}

	/**
	 * Setter
	 * 
	 * @param money
	 */
	public void setMoney(final BigDecimal money) {
		this.money = money;
	}

	/**
	 * Getter
	 * 
	 * @return
	 */
	public BigDecimal getMoney() {
		return this.money;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + ((money == null) ? 0 : money.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "Payment [currency=" + currency + ", money=" + money + "]";
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Payment other = (Payment) obj;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (money == null) {
			if (other.money != null)
				return false;
		} else if (!money.equals(other.money))
			return false;
		return true;
	}
	
}
