package com.testApp.tracker.writer;

import java.math.BigDecimal;

import com.testApp.tracker.domain.Payment;
import com.testApp.tracker.exchangeOffice.ExchangeOffice;
import com.testApp.tracker.loader.ConfigKeys;
import com.testApp.tracker.loader.Loader;
import com.testApp.tracker.storage.Storage;

/**
 * Once per minute one item will be displayed
 * 
 * @author
 *
 */
public class Listing implements Runnable {
	private Storage storage = null;
	private static ExchangeOffice exOffice;
	private final static String BASE_CURRENCY = Loader.getInstance().getValue(ConfigKeys.BASE_CURRENCY);
	private final static String DELIMITER = Loader.getInstance().getValue(ConfigKeys.DELIMITER);

	public void setStorage(final Storage storage) {
		this.storage = storage;
	}

	public void setExchangeOffice(final ExchangeOffice office) {
		exOffice = office;
	}

	@Override
	public void run() {
		System.out.println("-----------Payments----------");
		for (Payment item : this.storage.getAll().values()) {
			// zero amount will not be displayed
			if (item.getMoney() != null && item.getMoney().compareTo(BigDecimal.ZERO) > 0) {
				if (item.getCurrency().equalsIgnoreCase(BASE_CURRENCY)) {
					System.out.println(item.getCurrency() + " " + item.getMoney());
				} else {
					BigDecimal rate = exOffice.getExchangeRate(BASE_CURRENCY + DELIMITER + item.getCurrency());
					BigDecimal amount  = item.getMoney().divide(rate,2,2);
					System.out.println(
							item.getCurrency() + " " + item.getMoney() + " (" + amount + item.getCurrency() + ")");
				}
			}
		}
		System.out.println("-----------------------------");
	}
}