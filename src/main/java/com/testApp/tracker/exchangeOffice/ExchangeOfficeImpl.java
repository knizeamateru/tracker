package com.testApp.tracker.exchangeOffice;

import java.math.BigDecimal;

import java.util.Hashtable;
import java.util.Map;


public class ExchangeOfficeImpl implements ExchangeOffice {
	private static Map<String, BigDecimal> exchangeRatesTable = new Hashtable<String, BigDecimal>();


	@Override
	public BigDecimal getExchangeRate(final String currency) {
		return exchangeRatesTable.get(currency);
	}

	@Override
	public void setExchangeRate(final String currency, final BigDecimal value) {
		exchangeRatesTable.put(currency, value);
	}

	public Map<String, BigDecimal> getExchangeRatesTable() {
		return exchangeRatesTable;
	}

	@Override
	public boolean isCurrencyValid(final String currency) {
		if(exchangeRatesTable.containsKey(currency)){
			return Boolean.TRUE;
		}else{
			return Boolean.FALSE;
		}
	}

}
