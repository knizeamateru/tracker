package com.testApp.tracker.exchangeOffice;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Interface presents sample methods for operation with ExchangeOffice (Exchange rates table)
 * @author 
 *
 */
public interface ExchangeOffice {
	public BigDecimal getExchangeRate(String currency);
	public void setExchangeRate(final String currency, final BigDecimal value);
	public Map<String, BigDecimal> getExchangeRatesTable();
	public boolean isCurrencyValid(final String currency);
}
